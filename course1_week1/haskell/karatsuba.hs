-- | Get the digits of an integer one by one and 
-- add them to a list.
intToInts' :: Integer -> [Integer]
intToInts' x
  | (x > 9)   = mod (div x 1) 10 : (intToInts' (div x 10))
  | otherwise = [x]


-- | Reverse the list so the digits stay in order
intToInts :: Integer -> [Integer]
intToInts x = reverse $ intToInts' x


-- | Get an Integer from a list of Integers
listToInteger :: [Integer] -> Integer
listToInteger x = foldl addDigit 0 x
  where addDigit num d = 10*num + d

 
-- | split a number array in two by half
splitInTwo :: [Integer] -> ([Integer], [Integer])
splitInTwo x = (x1, x2)
  where
    n_digits = length x
    x1       = take (div (length x) 2) x
    x2       = drop (div (length x) 2) x


listToListOfChars :: [Integer] -> String
listToListOfChars x = map (toChars) x
  where toChars x1 | x1 == 0 = '0'
                   | x1 == 1 = '1'
                   | x1 == 2 = '2'
                   | x1 == 3 = '3'
                   | x1 == 4 = '4'
                   | x1 == 5 = '5'
                   | x1 == 6 = '6'
                   | x1 == 7 = '7'
                   | x1 == 8 = '8'
                   | x1 == 9 = '9'


stringToIntegers :: String -> [Integer]
stringToIntegers x = map toIntegers x
  where toIntegers x1 | x1 == '0' = 0
                      | x1 == '1' = 1
                      | x1 == '2' = 2
                      | x1 == '3' = 3
                      | x1 == '4' = 4
                      | x1 == '5' = 5
                      | x1 == '6' = 6
                      | x1 == '7' = 7
                      | x1 == '8' = 8
                      | x1 == '9' = 9


karatsuba' :: [Integer] -> [Integer] -> Integer
karatsuba' [x1] y = x1 * (listToInteger y)
karatsuba' x [y1] = (listToInteger x) * y1
karatsuba' x y = 10^n * ac + 10^(div n 2) * (ad + bc) + bd
  where
    n  = length x 
    ac = karatsuba' ((fst . splitInTwo) x) ((fst . splitInTwo) y)
    ad = karatsuba' ((fst . splitInTwo) x) ((snd . splitInTwo) y)
    bc = karatsuba' ((snd . splitInTwo) x) ((fst . splitInTwo) y)
    bd = karatsuba' ((snd . splitInTwo) x) ((snd . splitInTwo) y)

karatsuba :: Integer -> Integer -> Integer
karatsuba x y = karatsuba' (intToInts x) (intToInts y)


-- | Multiply two numbers given by the user
main :: IO ()
main = do
  putStrLn "Karatsuba algorithm. Escribe dos numeros"
  x <- getLine
  y <- getLine
  putStrLn (x ++ " * " ++ y ++ " = " ++ listToListOfChars (intToInts 
    (karatsuba ((listToInteger . stringToIntegers) x) 
               ((listToInteger . stringToIntegers) y))))
