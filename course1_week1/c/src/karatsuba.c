/**
 * @file karatsuba.c
 * @brief Multiply two same digit numbers with karatsuba algorithm
 *
 * The code is written to handle integers, and since long long can't fit
 * really big numbers, the program is somewhat limited. The better alternative, 
 * which is taken in the python version is to interpret those numbers as strings.
 *
 * @author Luis Domingo Aranda
 * @date May 05 2019
 * @see https://en.wikipedia.org/wiki/Karatsuba_algorithm
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Function declarations
int count_digits(int x);
int kat_mult(int x, int y);

int main(int argc, char* argv[])
{
        int x = atoi(argv[1]);
        int y = atoi(argv[2]);

        int mult = kat_mult(x, y);

        printf("x:   %10d\n" 
               "y:   %10d\n"
               "x*y: %10d\n", x, y, mult);

        return 0;
}

/**
 * @brief Count how many digits an integer has
 *
 * Take an integer and divide it by 10. Neglect the remainder.
 * You keep doing that, until you get a 0.
 * The number of divisions you've done is the number of integers.
 *
 * @param[in] x        Integer
 * @return    n_digits Integer
 */
int count_digits(int x)
{
        int count = 0;
        while( x != 0 ) {
                x /= 10;
                ++count;
        }
        return count;
}

/** 
 * @brief Karatsuba multiplication algorithm
 *
 * Computes the multiplication of two same digit integers recursively as:
 * \f$x y = 10^n ac + 10^{\frac{n}{2}} (ad + bc) + bd\f$
 * where \f$ n \f$ is the number of digits of both \f$ x \f$ and \f$ y \f$.
 * 
 * @param[in]   x       Integer1
 * @param[in]   y       Integer2
 * @return      Multiplication result
 */
int kat_mult(int x, int y)
{
        static int n_pass = 0;  // first recursive pass
        int x_digits = count_digits(x);
        int y_digits = count_digits(y);
        int n_digits = (x_digits > y_digits) ? x_digits : y_digits;

        /// Karatsuba algorithm restriction on first pass
        if (n_pass == 0) {
                if (x_digits != y_digits) {
                        fprintf(stderr, "Problem at line %d, in file %s: \n"
                                        "Integer dimensions don't match  \n"
                                                        , __LINE__, __FILE__);
                        exit(EXIT_FAILURE);
                }
        }
        n_pass++;

        // Define variables
        int a = 0 
          , b = 0
	  , c = 0
	  , d = 0;

        int ac = 0
  	  , ad = 0 
          , bc = 0
       	  , bd = 0;

        // Set variables
        for (int i = x_digits; i > x_digits/2; --i) {
                // 123 = 1*100 + 2*10 + 3*1
                a += (x / (int)pow(10, x_digits-(i-x_digits/2)) % 10) * \
                                                (int)pow(10, x_digits-i);
                b += (x / (int)pow(10, x_digits-i) % 10) * \
                                        (int)pow(10, x_digits-i);
        }

        for (int i = y_digits; i > y_digits/2; --i) {
                // 123 = 1*100 + 2*10 + 3*1
                c += (y / (int)pow(10, y_digits-(i-y_digits/2)) % 10) * \
                                                (int)pow(10, y_digits-i);
                d += (y / (int)pow(10, y_digits-i) % 10) * \
                                        (int)pow(10, y_digits-i);
        }

        // If both numbers in the multiplication are of length 1, 
        // multiply them directly
        if (count_digits(a) > 1 && count_digits(b) > 1) {
                ac = kat_mult(a, c);
        } else {
                ac = a * c;
        }

        if (count_digits(a) > 2 && count_digits(d) > 1) {
                ad = kat_mult(a, d);
        } else {
                ad = a * d;
        }

        if (count_digits(b) > 1 && count_digits(c) > 1) {
                bc = kat_mult(b, c);
        } else {
                bc = b * c;
        }

        if (count_digits(b) > 1 && count_digits(d) > 1) {
                bd = kat_mult(b, d);
        } else {
                bd = b * d;
        }

        int result = pow(10, n_digits) * ac
                   + pow(10, n_digits/2) * (ad + bc)
                   + bd;

        return result;
}
